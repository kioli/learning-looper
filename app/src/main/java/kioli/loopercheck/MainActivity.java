package kioli.loopercheck;

import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

/*
* When the activity starts for the first time, a LAUNCH_ACTIVITY message is posted to the looper queue
*
* When processed, LAUNCH_ACTIVITY creates the activity instance, calls onCreate() and then onResume() all in a row.
*
* When a device orientation change is detected, a RELAUNCH_ACTIVITY is posted to the queue.
*
* When that message is processed, it:
*           - calls onSaveInstanceState(), onPause(), onDestroy() on the old activity instance
*           - creates a new activity instance and calls on it onCreate() and onResume()
*
* (every message posted in the meantime will be handled after onResume() of the new activity has been called)
*
****
*
* So when you post a message you have no guarantee that the activity instance that existed at the time it was sent
* will still be running when the message is handled (even if you post from onCreate() or onResume()).
*
* If your message holds a reference to a view or an activity, those won't be garbage collected until the message is handled
* leading to memory leaks
* */
public class MainActivity extends Activity {

    private static final String TAG = "TAG ACTIVITY";

    private final Spy mainLooperSpy = new Spy();
    private final Handler handler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate()");
        if (savedInstanceState == null) {
            handler.post(new Runnable() {
                public void run() {
                    Log.d(TAG, "Posted before requesting orientation change");
                }
            });

            Log.d(TAG, "Requesting orientation change");
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

            handler.post(new Runnable() {
                public void run() {
                    Log.d(TAG, "Posted after requesting orientation change");
                }
            });

            mainLooperSpy.dumpQueue();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "onResume()");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, "onPause()");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy()");
    }
}
