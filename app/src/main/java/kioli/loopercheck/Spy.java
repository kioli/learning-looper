package kioli.loopercheck;

import android.os.Looper;
import android.os.Message;
import android.os.MessageQueue;
import android.util.Log;

import java.lang.reflect.Field;

/*
* Class to read the content of the looper queue via reflection
* */
public class Spy {

    private static final String TAG = "TAG SPY";

    public static final int CONFIGURATION_CHANGED   = 118;
    public static final int RELAUNCH_ACTIVITY       = 126;
    public static final int ENTER_ANIMATION_COMPLETE = 149;

    private final Field messagesField;
    private final Field nextField;
    private final MessageQueue mainMessageQueue;

    public Spy() {
        try {
            Field queueField = Looper.class.getDeclaredField("mQueue");
            queueField.setAccessible(true);

            messagesField = MessageQueue.class.getDeclaredField("mMessages");
            messagesField.setAccessible(true);

            nextField = Message.class.getDeclaredField("next");
            nextField.setAccessible(true);

            mainMessageQueue = (MessageQueue) queueField.get(Looper.getMainLooper());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public void dumpQueue() {
        try {
            Log.d(TAG, "Begin dumping queue");
            dumpMessages((Message) messagesField.get(mainMessageQueue));
            Log.d(TAG, "End dumping queue");
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    private void dumpMessages(Message message) throws IllegalAccessException {
        if (message != null) {
            switch (message.what) {
                case CONFIGURATION_CHANGED:
                    Log.d(TAG, "Message: CONFIGURATION CHANGED");
                    break;
                case ENTER_ANIMATION_COMPLETE:
                    Log.d(TAG, "Message: ENTER_ANIMATION_COMPLETE");
                    break;
                case RELAUNCH_ACTIVITY:
                    Log.d(TAG, "Message: RELAUNCH_ACTIVITY");
                    break;
                default:
                    Log.d(TAG, "Message: " + message.toString());
                    break;
            }

            dumpMessages((Message) nextField.get(message));
        }
    }
}
